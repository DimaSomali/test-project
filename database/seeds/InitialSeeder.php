<?php

use App\Models\Client;
use Illuminate\Database\Seeder;

class InitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'       => 'test',
            'email'      => 'admin@local.host',
            'password'   => Hash::make('123123'),
            'created_at' => '2018-01-01 00:00:00',
            'updated_at' => '2018-01-01 00:00:00',
        ]);

        DB::table('clients')->insert([
            'name'       => 'test Client',
            'created_at' => '2018-01-01 00:00:00',
            'updated_at' => '2018-01-01 00:00:00',
        ]);

        $client = Client::first();

        DB::table('projects')->insert([
            'name'       => 'test Project',
            'client_id'  => $client->id,
            'created_at' => '2018-01-01 00:00:00',
            'updated_at' => '2018-01-01 00:00:00',
        ]);

        DB::table('tasks')->insert([
            'name'       => 'test Task',
            'created_at' => '2018-01-01 00:00:00',
            'updated_at' => '2018-01-01 00:00:00',
        ]);
    }
}
