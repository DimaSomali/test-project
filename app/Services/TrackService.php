<?php

namespace App\Services;

use App\Models\Track;

class TrackService
{

    /**
     * Save new track.
     *
     * @param $request
     * @return Track $track
     */
    public function store($request)
    {
        $track = new Track($request->all());

        $track->user_id = $request->user_id;
        $track->project_id = $request->project_id;
        $track->task_id = $request->task_id;
        $track->start = $request->start;
        $track->stop = $request->stop;
        $track->duration = (null === $request->stop) ? null : $request->stop - $request->start;

        $track->save();

        return $track;
    }

    public function getTracks($user, $request)
    {
        $count = $request->has('count') ? $request->count : 10;
        $offset = $request->has('offset') ? $request->offset : 0;
        $beforeSendTime = $request->has('before_send_time') ? $request->before_send_time : null;
        $sinceSendTime = $request->has('since_send_time') ? $request->since_send_time : null;

        $tracks = Track::where('user_id', $user->id);

        if (null !== $sinceSendTime) {
            $tracks = $tracks->where('start','>=', $sinceSendTime);
        }

        if (null !== $beforeSendTime) {
            $tracks = $tracks->where('start', '<=', $beforeSendTime);
        }

        $tracks = $tracks->skip($offset)->take($count)->get();

        $tracks = $this->serializeTracks($tracks);

        return $tracks;
    }


    protected function serializeTracks($tracks)
    {
        $tracksCollection = collect();

        foreach ($tracks as $track) {

            $collection['track'] = $track->only(['id', 'duration', 'start', 'stop', 'description']);
            $collection['user'] = $track->author->only(['id', 'email', 'name']);
            $collection['project'] = $track->project->only(['id', 'name']);
            $collection['task'] = $track->task->only(['id', 'name']);
            $collection['client'] = $track->project->client->only(['id', 'name']);

            $tracksCollection->push($collection);
        }

        return $tracksCollection;
    }
}
