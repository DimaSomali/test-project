<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        "name",
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function tracks()
    {
        return $this->hasMany(Track::class);
    }
}
