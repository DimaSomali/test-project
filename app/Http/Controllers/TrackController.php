<?php

namespace App\Http\Controllers;

use App\Http\Requests\TrackStoreRequest;
use App\Models\Project;
use App\Models\Task;
use App\Models\Track;
use App\Models\User;
use App\Services\TrackService;
use Illuminate\Http\Request;

class TrackController extends Controller
{
    protected $trackService;


    public function __construct(TrackService $trackService)
    {
        $this->trackService = $trackService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrackStoreRequest $request)
    {
        $track = $this->trackService->store($request);

        if (null === $track) {
            $response['message'] = 'Something went wrong';

            return response()->json($response, 500);
        }

        $response['message'] = 'Track has been saved!';

        return response()->json($response, 200);
    }


    public function userTracks($userId, Request $request)
    {
        $user = User::where('id', $userId)->first();

        if (null === $user) {
            $response['message'] = 'User cannot be found';
            return response()->json($response, 403);
        }

        $tracks = $this->trackService->getTracks($user, $request);

        return response()->json($tracks, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
