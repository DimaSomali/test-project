<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class TrackStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'      => 'required|exists:users,id',
            'project_id'   => 'required|exists:projects,id',
            'task_id'      => 'required|exists:tasks,id',
            'description'  => 'nullable|string|max:255',
            'start'        => 'required|regex:/^[0-9]*$/',
            'stop'         => 'nullable|regex:/^[0-9]*$/',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required'     => 'User ID is required.',
            'project_id.required'  => 'Project ID is required.',
            'task_id.required'     => 'Task ID is required.',
            'user_id.exists'       => 'User cannot be found',
            'project_id.exists'    => 'Project cannot be found',
            'task_id.exists'       => 'Task cannot be found',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(['errors' => $errors
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
